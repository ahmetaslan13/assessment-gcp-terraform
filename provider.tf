# Define the Google Cloud provider configuration
provider "google" {
  credentials = file("./creds/serviceaccount.json") # Path to the service account JSON key file
  project     = "gcp-terraform-nginx-397300"        # Replace with your project ID
  region      = "US"                                # Specify the desired region
}
