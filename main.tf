# # Network Module
# module "network" {
#   source                  = "./modules/compute_network"
#   name                    = var.network_name
#   auto_create_subnetworks = var.auto_create_subnetworks
# }

# # Public Subnet Module
# module "web_subnetwork" {
#   source        = "./modules/compute_subnetwork"
#   name          = "web-${var.subnetwork_name}"
#   ip_cidr_range = var.web_ip_cidr_range
#   region        = var.web_region
#   network       = module.network.self_link
#   depends_on    = [module.network]
# }

# # Private Subnet Module
# module "db_subnetwork" {
#   source        = "./modules/compute_subnetwork"
#   name          = "db-${var.subnetwork_name}"
#   ip_cidr_range = var.db_ip_cidr_range
#   region        = var.db_region
#   network       = module.network.self_link
# }

# # Public Instance Module
# module "web_instance" {
#   source       = "./modules/public_instance"
#   name         = var.web_instance_name
#   machine_type = var.web_instance_machine_type
#   zone         = var.web_instance_zone
#   image        = var.web_instance_image
#   network      = module.network.self_link
#   subnetwork   = module.web_subnetwork.self_link
#   tags         = var.web_nginx_instance_tags
#   depends_on   = [module.network, module.db_subnetwork]
# }

# # Private Instance Module
# module "db_instance" {
#   source       = "./modules/private_instance"
#   name         = var.db_instance_name
#   machine_type = var.db_instance_machine_type
#   zone         = var.db_instance_zone
#   image        = var.db_instance_image
#   network      = module.network.self_link
#   subnetwork   = module.db_subnetwork.self_link
#   tags         = var.db_nginx_instance_tags
#   depends_on   = [module.network, module.db_subnetwork]
# }

# # Router Module
# module "nginx-router" {
#   source  = "./modules/router"
#   name    = var.router_name
#   region  = module.web_subnetwork.region
#   network = module.network.id
# }

# # NAT Module
# module "nginx-router-nat" {
#   source                             = "./modules/nat"
#   name                               = var.nat_name
#   router                             = module.nginx-router.name
#   region                             = module.web_subnetwork.region
#   nat_ip_allocate_option             = var.nat_ip_allocate_option
#   source_subnetwork_ip_ranges_to_nat = var.source_subnetwork_ip_ranges_to_nat
#   depends_on                         = [module.nginx-router, module.db_subnetwork]
# }

# # Firewall Module for Public Subnet
# module "web-allow-firewall" {
#   source        = "./modules/firewall"
#   name          = var.web_allow_firewall_name
#   network       = module.network.self_link
#   protocol      = var.web_allow_protocol
#   ports         = var.web_allow_ports
#   target_tags   = var.web_allow_target_tags
#   source_ranges = var.web_allow_source_ranges
# }

# # Firewall Module for Private Subnet
# module "db-allow-firewall" {
#   source        = "./modules/firewall"
#   name          = var.db_allow_firewall_name
#   network       = module.network.self_link
#   protocol      = var.db_allow_protocol
#   ports         = var.db_allow_ports
#   target_tags   = var.db_allow_target_tags
#   source_ranges = var.db_allow_source_ranges
# }
