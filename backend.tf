# Define the required Terraform version and backend configuration
terraform {
  required_version = "~>1.4.2"

  # Configure the Google Cloud Storage (GCS) backend
  backend "gcs" {
    credentials = "./creds/serviceaccount.json" # Path to the service account JSON key file
    bucket      = "gcp_terraform_nginx_bucket"  # Name of the GCS bucket
  }
}
