variable "network_name" {
  description = "The name of the network to be created."
  type        = string
}

variable "auto_create_subnetworks" {
  description = "Whether to automatically create subnetworks within the network. Set to true to enable automatic subnetwork creation."
  type        = bool
}

variable "subnetwork_name" {
  description = "The name of the subnetwork to be created."
  type        = string
}

variable "web_ip_cidr_range" {
  description = "The IP address range in CIDR notation for the main network."
  type        = string
}

variable "db_ip_cidr_range" {
  description = "The IP address range in CIDR notation for private IP addresses within the subnetwork."
  type        = string
}

variable "web_region" {
  description = "The region in which to create the network and related resources."
  type        = string
}

variable "db_region" {
  description = "The region in which to create the network and related resources."
  type        = string
}

variable "web_instance_image" {
  description = "The image to be used for provisioning the instance."
  type        = string
}

variable "web_instance_machine_type" {
  description = "The machine type for the provisioned instance."
  type        = string
}

variable "web_instance_name" {
  description = "The name of the provisioned instance."
  type        = string
}

variable "web_instance_zone" {
  description = "The availability zone for the provisioned instance."
  type        = string
}

variable "web_nginx_instance_tags" {
  type = any
}

//DB Instance

variable "db_instance_image" {
  description = "The image to be used for provisioning the instance."
  type        = string
}

variable "db_instance_machine_type" {
  description = "The machine type for the provisioned instance."
  type        = string
}

variable "db_instance_name" {
  description = "The name of the provisioned instance."
  type        = string
}

variable "db_instance_zone" {
  description = "The availability zone for the provisioned instance."
  type        = string
}

variable "db_nginx_instance_tags" {
  type = any
}

//Router

variable "router_name" {
  description = "The name of the network"
  type        = string
}

variable "router_region" {
  type = string
}

// NAT
variable "nat_name" {
  description = "The name of the network"
  type        = string
}
variable "nat_ip_allocate_option" {
  description = "The name of the network"
  type        = string
}
variable "source_subnetwork_ip_ranges_to_nat" {
  description = "The name of the network"
  type        = string
}

// Firewall - Web Allow

variable "web_allow_firewall_name" {
  description = "The name of the network"
  type        = string
}

variable "web_allow_protocol" {
  description = "Whether to automatically create subnetworks within the network"
  type        = any
}

variable "web_allow_ports" {
  description = "Whether to automatically create subnetworks within the network"
  type        = any
}

variable "web_allow_target_tags" {
  description = "Whether to automatically create subnetworks within the network"
  type        = any
}

variable "web_allow_source_ranges" {
  description = "Whether to automatically create subnetworks within the network"
  type        = any
}

// Firewall - Db Allow


variable "db_allow_firewall_name" {
  description = "The name of the network"
  type        = string
}

variable "db_allow_protocol" {
  description = "Whether to automatically create subnetworks within the network"
  type        = any
}

variable "db_allow_ports" {
  description = "Whether to automatically create subnetworks within the network"
  type        = any
}

variable "db_allow_target_tags" {
  description = "Whether to automatically create subnetworks within the network"
  type        = any
}

variable "db_allow_source_ranges" {
  description = "Whether to automatically create subnetworks within the network"
  type        = any
}
