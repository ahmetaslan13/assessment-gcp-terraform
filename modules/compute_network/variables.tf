variable "name" {
  description = "The name of the network."
  type        = string
}

variable "auto_create_subnetworks" {
  description = "Whether to automatically create subnetworks within the network."
  type        = bool
}
