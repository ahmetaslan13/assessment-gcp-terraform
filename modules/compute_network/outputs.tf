output "self_link" {
  value       = google_compute_network.vpc.self_link
  description = "The self-link identifier of the VPC network."
}

output "id" {
  value       = google_compute_network.vpc.id
  description = "The unique identifier of the VPC network."
}
