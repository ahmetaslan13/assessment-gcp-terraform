variable "name" {
  description = "The name of the bucket. (Required)"
  type        = string
}

variable "ip_cidr_range" {
  description = "The IP CIDR range for the subnet. Can be a string or a list of strings."
  type        = any
}

variable "region" {
  description = "The region where the resource will be located."
  type        = string
}

variable "network" {
  description = "The network to which the subnet will be attached. Can be a string or an object reference."
  type        = any
}
