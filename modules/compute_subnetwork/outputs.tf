output "self_link" {
  value       = google_compute_subnetwork.subnet.self_link
  description = "The self-link identifier of the subnet."
}

output "region" {
  value       = google_compute_subnetwork.subnet.region
  description = "The region where the subnet is located."
}
