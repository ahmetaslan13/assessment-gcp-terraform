variable "name" {
  description = "A descriptive name for the virtual machine instance."
  type        = string
}

variable "image" {
  description = "The ID or URL of the machine image to use for the instance."
  type        = string
}

variable "machine_type" {
  description = "The machine type to use for the instance."
  type        = string
}

variable "zone" {
  description = "The zone where the instance will be created."
  type        = string
}

variable "network" {
  description = "The name of the network to which the instance will be connected."
  type        = string
}

variable "subnetwork" {
  description = "The name of the subnetwork to which the instance will be attached."
  type        = string
}

variable "tags" {
  description = "A list of tags to associate with the instance."
  type        = any
}
