output "self_link" {
  value       = google_compute_instance.public_instance.self_link
  description = "The self-link identifier of the public instance."
}
