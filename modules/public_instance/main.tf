# Define a Google Compute Instance for Public Use
resource "google_compute_instance" "public_instance" {
  name         = var.name
  machine_type = var.machine_type
  zone         = var.zone
  tags         = var.tags

  # Define the boot disk with the specified image
  boot_disk {
    initialize_params {
      image = var.image
    }
  }

  # Define the network interface configuration
  network_interface {
    network = var.network
    subnetwork = var.subnetwork

    # Configure access settings for the instance
    access_config {
     
    }
  }

  # Define the startup script for NGINX
  metadata_startup_script = <<-EOF
    #!/bin/bash
    sudo apt-get update
    sudo apt-get install -y nginx
    sudo service nginx start
  EOF
}
