variable "name" {
  description = "The name of the network."
  type        = string
}

variable "network" {
  description = "The network in which to create the firewall rule."
  type        = any
}

variable "protocol" {
  description = "The protocol to which the firewall rule applies (e.g., 'tcp', 'udp', 'icmp')."
  type        = any
}

variable "ports" {
  description = "The list of ports to which the firewall rule applies."
  type        = any
}

variable "target_tags" {
  description = "The list of target tags to associate with the firewall rule."
  type        = any
}

variable "source_ranges" {
  description = "The list of source IP ranges to allow for the firewall rule."
  type        = any
}
