# Define a Google Compute Firewall for Allowing Web Traffic
resource "google_compute_firewall" "allow-web" {
  name    = var.name
  network = var.network

  # Allow specific protocol and ports
  allow {
    protocol = var.protocol
    ports    = var.ports
  }
  
  # Define target tags for the firewall rule
  target_tags   = var.target_tags
  
  # Define source IP ranges allowed by the firewall rule
  source_ranges = var.source_ranges
}
