output "self_link" {
  value       = google_compute_firewall.allow-web.self_link
  description = "The self-link identifier of the firewall rule."
}

output "id" {
  value       = google_compute_firewall.allow-web.id
  description = "The unique identifier of the firewall rule."
}
