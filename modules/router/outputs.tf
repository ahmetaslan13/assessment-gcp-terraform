output "self_link" {
  value       = google_compute_router.router.self_link
  description = "The self-link identifier of the router."
}

output "id" {
  value       = google_compute_router.router.id
  description = "The unique identifier of the router."
}

output "region" {
  value       = google_compute_router.router.region
  description = "The region where the router is located."
}

output "name" {
  value       = google_compute_router.router.name
  description = "The name of the router."
}
