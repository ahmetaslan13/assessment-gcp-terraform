# Define a Google Compute Router
resource "google_compute_router" "router" {
  name    = var.name
  region  = var.region
  network = var.network
  
  # Define BGP settings for the router
  bgp {
    asn = 64514
  }
}