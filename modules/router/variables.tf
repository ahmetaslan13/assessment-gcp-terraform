variable "name" {
  description = "A descriptive name for the network."
  type        = string
}

variable "region" {
  description = "The region where the network will be created."
  type        = string
}

variable "network" {
  description = "The name of the parent network."
  type        = string
}
