output "self_link" {
  value       = google_compute_router_nat.nat.id
  description = "The self-link identifier of the NAT router."
}

output "region" {
  value       = google_compute_router_nat.nat.region
  description = "The region where the NAT router is located."
}
