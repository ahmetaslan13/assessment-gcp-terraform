# Define a Google Compute NAT router configuration
resource "google_compute_router_nat" "nat" {
  name                               = var.name
  router                             = var.router
  region                             = var.region
  nat_ip_allocate_option             = var.nat_ip_allocate_option
  source_subnetwork_ip_ranges_to_nat = var.source_subnetwork_ip_ranges_to_nat
  
  # Configure NAT log settings
  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}
