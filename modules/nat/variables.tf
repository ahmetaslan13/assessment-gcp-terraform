variable "name" {
  description = "The name of the resource."
  type        = string
}

variable "router" {
  description = "The name of the router to configure NAT settings."
  type        = string
}

variable "region" {
  description = "The region where the resource will be located."
  type        = string
}

variable "nat_ip_allocate_option" {
  description = "The method of NAT IP allocation. Options: 'AUTO_ONLY', 'MANUAL_ONLY', 'AUTO_ONLY_NO_CLOUD_ROUTING'."
  type        = string
}

variable "source_subnetwork_ip_ranges_to_nat" {
  description = "List of subnetwork IP ranges that use auto NAT or custom NAT configuration."
  type        = string
}
