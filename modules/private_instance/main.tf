# Define a Google Compute Instance for Private Use
resource "google_compute_instance" "private_instance" {
  name         = var.name
  machine_type = var.machine_type
  zone         = var.zone
  tags         = var.tags

  # Define the boot disk with the specified image
  boot_disk {
    initialize_params {
      image = var.image
    }
  }

  # Define the network interface configuration
  network_interface {
    network    = var.network
    subnetwork = var.subnetwork
  }
}
