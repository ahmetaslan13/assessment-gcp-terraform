output "self_link" {
  value       = google_compute_instance.private_instance.self_link
  description = "The self-link identifier of the private instance."
}
