// VPC configuration
network_name            = "dev-nginx"            // Name of the VPC network
auto_create_subnetworks = false                  // Disable automatic subnetwork creation
network_region          = "us-west1"             // Region for the VPC network

// Subnet configuration
subnetwork_name   = "ngingx"                     // Name of the subnetwork
web_ip_cidr_range = "10.2.0.0/16"                // IP CIDR range for the web subnet
db_ip_cidr_range  = "10.3.0.0/16"                // IP CIDR range for the database subnet
web_region        = "us-west1"                   // Region for the web subnet
db_region         = "us-west1"                   // Region for the database subnet

// Web Instance configuration
web_instance_name         = "web-nginx"           // Name of the web instance
web_instance_machine_type = "n1-standard-1"       // Machine type for the web instance
web_instance_zone         = "us-west1-a"          // Zone for the web instance
web_instance_image        = "ubuntu-os-cloud/ubuntu-2004-lts"  // Image for the web instance
web_nginx_instance_tags   = ["web"]               // Tags for the web instance

// Public Instance configuration
db_instance_name         = "db-nginx"             // Name of the database instance
db_instance_machine_type = "n1-standard-1"        // Machine type for the database instance
db_instance_zone         = "us-west1-a"           // Zone for the database instance
db_instance_image        = "ubuntu-os-cloud/ubuntu-2004-lts"  // Image for the database instance
db_nginx_instance_tags   = []                     // Tags for the database instance

// Router configuration
router_region = "us-west1"                        // Region for the router
router_name   = "nginx-router"                   // Name of the router

// NAT configuration
nat_name                           = "nginx-router-nat"   // Name of the NAT
nat_ip_allocate_option             = "AUTO_ONLY"           // IP allocation option for NAT
source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"  // Source IP ranges for NAT

// Web Allow Firewall configuration
web_allow_firewall_name    = "allow-web"           // Name of the firewall rule for web traffic
web_allow_protocol = "tcp"                        // Protocol allowed by the firewall rule
web_allow_ports    = ["22", "80"]                 // Ports allowed by the firewall rule
web_allow_target_tags   = ["web"]                 // Target tags for the firewall rule
web_allow_source_ranges = ["0.0.0.0/0"]           // Source IP ranges allowed by the firewall rule

// DB Allow Firewall configuration
db_allow_firewall_name    = "allow-db-ssh"        // Name of the firewall rule for database SSH
db_allow_protocol = "tcp"                         // Protocol allowed by the firewall rule
db_allow_ports   = ["22"]                         // Ports allowed by the firewall rule
db_allow_target_tags   = ["db"]                   // Target tags for the firewall rule
db_allow_source_ranges = ["0.0.0.0/0"]            // Source IP ranges allowed by the firewall rule
